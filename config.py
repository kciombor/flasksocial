import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
	SECURITY_POST_LOGIN_VIEW = '/profile'
	SECURITY_REGISTERABLE = True
	SECRET_KEY = 'KGLUCDT@O5[Cf1S8y)Kt6`UoTbMi8Y'
	SOCIAL_TWITTER = {
		'consumer_key': '7QLFDRIPe6iZot4LR7gIJ7oMo',
		'consumer_secret': 'MtOQIiJulHJti7vPZPsG8ogvWDQWsve3K0BnAcwqqYRLFtj1pJ'
	}
	
	@staticmethod
	def init_app(app):
		pass

class DevelopementConfig(Config):
	DEBUG = True
	SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'feedbook.db')

config = {
	'developement': DevelopementConfig,
	'default': DevelopementConfig
}