from wtforms import Form, TextField, HiddenField

class FeedForm(Form):
	feed = TextField('Feed')
	board_id = HiddenField('board_id')

class BoardForm(Form):
	name = TextField('Name')
	description = TextField('Description')