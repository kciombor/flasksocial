from . import main
from .. import db
from ..models import Board, Feed, Connection
from .forms import FeedForm, BoardForm
from flask import current_app
from flask import url_for, request, redirect
from flask.ext.security import login_required, current_user
from flask import render_template

@main.route("/")
def index():
	return "Hello"

@main.route("/connect/twitter/<access_token>")
@login_required
def disconnect(access_token):
	print access_token
	connection = Connection.query.filter_by(provider_user_id=access_token).first()
	print connection
	db.session.delete(connection)
	db.session.commit()
	return redirect("/profile")

@main.route("/admin")
def create_user():
    current_app.user_datastore.create_user(email='test@test.com', password='test')
    db.session.commit()
    return "OK"

@main.route('/profile')
@login_required
def profile():
	return render_template(
		'profile.html',
		content='Profile Page',
		twitter_conn=current_app.social.twitter.get_connection())

@main.route("/boards")
@login_required
def list_boards():
	boards = Board.query.all()
	return render_template('list_boards.html', boards = boards)

@main.route("/create_board", methods = ['GET', 'POST'])
@login_required
def create_board():
	form = BoardForm(request.form)
	if request.method == 'POST' and form.validate():
		name = form.name.data
		description = form.description.data
		board = Board(name, description)
		db.session.add(board)
		db.session.commit()
		return redirect("/feed")
	return render_template('create_board.html', form = form)

@main.route("/board/<int:board_id>")
def show_board(board_id):
	board = Board.query.get(board_id)
	form = FeedForm(request.form)
	form.board_id.data = board_id
	return render_template("board.html", board = board, form = form)

@main.route("/feed", methods = ['GET', 'POST'])
@login_required
def feed():
	form = FeedForm(request.form)
	if request.method == 'POST' and form.validate():
		body = form.feed.data
		board_id = form.board_id.data
		board = Board.query.get(board_id)
		feed = Feed(body, board_id, current_user.get_id())
		db.session.add(feed)
		db.session.commit()

		conn = current_app.social.twitter.get_connection()
		if conn:
			hashTag = "#"+board.name
			body += " " + hashTag
			twitter_api = current_app.social.twitter.get_api()
			twitter_api.PostUpdate(body)

		return redirect("/board/"+board_id)
	return render_template('feed.html', form = form)