from flask import Flask, render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.social import Social
from flask.ext.social.datastore import SQLAlchemyConnectionDatastore
from flask.ext.security import Security, SQLAlchemyUserDatastore
from config import config

db = SQLAlchemy()

def create_app(config_name):
	app = Flask(__name__)
	app.config.from_object(config[config_name])
	config[config_name].init_app(app)

	db.init_app(app)

	from models import User, Role, Connection
	user_datastore = SQLAlchemyUserDatastore(db, User, Role)
	app.user_datastore = user_datastore
	security = Security(app, user_datastore)
	app.social = Social(app, SQLAlchemyConnectionDatastore(db, Connection)) 

	from .main import main as main_blueprint
	app.register_blueprint(main_blueprint)

	@app.before_first_request
	def initialize_database():
		db.create_all()

	return app